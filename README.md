# README #

# **Co jest potrzebne do uruchomienia?** #

* Zainstalowany composer https://getcomposer.org/download/
* Apache 
* Baza MySQL

**Lub**

* Gotowa paczka Apache + MySQL np **xampp**

# **Jak uruchomić projekt?** #
* Po zainstalowaniu ww narzędzi przejdź do katalogu htdocs
* rozpakuj projekt w katalogu htdocs
* otworz terminal (np pod windowsem komenda cmd)
* przejdź do katalogu gdzie rozpakowałeś projekt (cd htdocs/nazwa_projektu)
* w terminalu uruchom komende **composer install**
* composer powinien zaistalować u Ciebie biblioteke zenda i inne potrzebne do uruchomienia projektu
* następnie trzeba zrobić migracje bazy danych czyli w terminalu wpisać komende **php public/index.php migrations:migrate**
* W tym momencie powinienes miec juz u siebie zainstalowaną baze danych
* Przejdź do bazy danych otwórz tabele user i dodaj do niej nowego użytkownika. W pole hasło wpisz wartość zakodowaną bcryptem czyli np możesz wygenerować go tutaj: http://bcrypthashgenerator.apphb.com/

W tym momencie masz gotowy zainstalowany projekt. Jednak lepiej jeszcze ustawić sobie virtual hosta
# **Aby to zrobić trzeba:** #
* Przejdź do katalogu conf gdzie jest zainstalowany Apache
* Znajdź sekcje gdzie są ustawiane VirtualHosty
* Doklej swojego:


```
#!php

<VirtualHost *:80>
    DocumentRoot "E:\zend\Apache2\htdocs\lignum\public"
    ServerName lignum.localhost
</VirtualHost>
```
* Ustaw poprawną ścieżke DocumentRoot do Twojego projektu

# **Ostatnia rzecz jaka pozostaje do zrobienia to ustawić hosta** #
1. Znajdź plik hosts (Pod windowsem: C:\Windows\System32\drivers\etc)
2. Dodaj linijke: 
```
#!php

127.0.0.1	lignum.localhost
```

Zrestartuj serwer Apache i wpisz url do przeglądarki: lignum.localhost powinieneś zobaczyć taką strone: http://lignum.com.pl/

Aby przejść do cms-a wpisz url: http://lignum.localhost/pl/auth i uzupełnij dane, które zapisałeś w bazie danych (email, password). Powinieneś się zalogować do cms-a gdzie możesz edytować dane. 