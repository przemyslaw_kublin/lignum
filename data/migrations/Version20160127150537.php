<?php

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160127150537 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE promotion CHANGE lang lang CHAR(3) NOT NULL, CHANGE valid_to valid_to DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE page CHANGE lang lang CHAR(3) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE page CHANGE lang lang CHAR(3) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE promotion CHANGE lang lang CHAR(3) NOT NULL COLLATE utf8_unicode_ci, CHANGE valid_to valid_to DATETIME DEFAULT NULL');
    }
}
