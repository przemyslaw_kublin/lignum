<?php
/**
 * Copyright: STORY DESIGN Sp. z o.o.
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 15:39
 */

namespace Products\Form;


use Products\Form\InputFilter\ProductInputFilter;
use Zend\Form\Form;

class ProductForm extends Form
{
    public function __construct()
    {
        parent::__construct('product-form');

        $this->setInputFilter(new ProductInputFilter());

        $this->setAttributes([
            'method' => 'post',
            'enctype'=> 'multipart/form-data'
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => _('Nazwa'),
            ],
            'attributes' => [
                'class' => 'form-control name',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => _('Opis'),
            ],
            'attributes' => [
                'class' => 'form-control description',
                'id' => 'editor'
            ],
        ]);

        $this->add([
            'name' => 'filename',
            'type' => 'file',
            'options' => [
                'label' => _('Obrazki (.jpg, .png)'),
            ],
            'attributes' => [
                'class' => 'form-control',
                'multiple' => "multiple"
            ],
        ]);
    }
}