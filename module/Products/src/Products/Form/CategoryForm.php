<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-29
 * Time: 12:51
 */

namespace Products\Form;


use Products\Form\InputFilter\CategoryInputFilter;
use Zend\Form\Form;

class CategoryForm extends Form
{
    public function __construct()
    {
        parent::__construct('product-category-form');

        $this->setInputFilter(new CategoryInputFilter());

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => _('Nazwa'),
            ],
            'attributes' => [
                'class' => 'form-control name',
            ],
        ]);
    }
}