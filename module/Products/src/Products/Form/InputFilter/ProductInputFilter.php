<?php
/**
 * Copyright: STORY DESIGN Sp. z o.o.
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 15:40
 */

namespace Products\Form\InputFilter;


use Zend\InputFilter\InputFilter;

class ProductInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'description',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);
    }
}