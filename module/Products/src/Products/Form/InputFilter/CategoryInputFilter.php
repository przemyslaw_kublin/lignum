<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-29
 * Time: 12:52
 */

namespace Products\Form\InputFilter;


use Zend\InputFilter\InputFilter;

class CategoryInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);
    }
}