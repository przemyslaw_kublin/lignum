<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-28
 * Time: 21:38
 */

namespace Products\Entity;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product_category")
 */
class Category extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="CHAR(3) NOT NULL")
     */
    protected $lang;

    /**
     * @ORM\Column(type="integer")
     */
    protected $position;

    /**
     * @ORM\OneToMany(targetEntity="Products\Entity\Product", mappedBy="category")
     */
    protected $product;

    /**
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $lang
     * @return Category
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $position
     * @return Category
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return Product[]
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return Category
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }
}