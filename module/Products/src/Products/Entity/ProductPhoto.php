<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 20:52
 */

namespace Products\Entity;


use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="product_image")
 */
class ProductPhoto extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Products\Entity\Product", inversedBy="photo")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $product;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $filename;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return ProductPhoto
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @param string $filename
     * @return ProductPhoto
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}