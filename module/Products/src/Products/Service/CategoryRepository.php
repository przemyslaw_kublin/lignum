<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-28
 * Time: 23:30
 */

namespace Products\Service;


use Cms\Service\AbstractRepository;

class CategoryRepository extends AbstractRepository
{
    /**
     * @param string $lang
     * @return array
     */
    public function getLastCategory($lang)
    {
        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('c')
            ->from('Products\Entity\Category', 'c')
            ->where('c.lang = :lang')
            ->orderBy('c.position', 'desc')
            ->setParameters(['lang' => $lang])
            ->setMaxResults(1)
        ;

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getAll($lang)
    {
        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('c')
            ->from('Products\Entity\Category', 'c')
            ->where('c.lang = :lang')
            ->orderBy('c.position', 'desc')
            ->setParameters(['lang' => $lang])
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}