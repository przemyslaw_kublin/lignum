<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 22.10.2015
 * Time: 11:03
 */

namespace Products;


return [
    'service_manager' => [
        'invokables' => [
            'Products\Service\CategoryRepository' => 'Products\Service\CategoryRepository',
        ],
    ],
];

