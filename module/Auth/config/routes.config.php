<?php

return [
    'router' => [
        'routes' => [
            'auth' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/auth',
                    'defaults' => [
                        'controller' => 'Auth\Controller\Index',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'login' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/login',
                            'defaults' => [
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/logout',
                            'defaults' => [
                                'action' => 'logout',
                            ],
                        ],
                    ],
                    'message' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/message',
                            'defaults' => [
                                'action' => 'message',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];