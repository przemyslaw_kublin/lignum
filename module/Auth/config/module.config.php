<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 22.10.2015
 * Time: 11:03
 */

namespace Auth;

return [
    'controllers' => [
        'invokables' => [
            'Auth\Controller\Index' => Controller\IndexController::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => false,
        'display_exceptions'       => false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/auth'           => __DIR__ . '/../view/layout/auth-layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
