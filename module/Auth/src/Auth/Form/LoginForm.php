<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-21
 * Time: 09:05
 */


namespace Auth\Form;

use Zend\Form\Form;
use \Auth\Form\InputFilter\LoginForm as LoginInputFilter;

class LoginForm extends Form {

    public function __construct()
    {
        parent::__construct('loginForm');

        $this->setInputFilter(new LoginInputFilter());

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'email',
            'type' => 'email',
            'options' => [
                'label' => 'Email',
            ],
            'attributes' => [
                'placeholder' => 'Email',
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'password',
            'type' => 'password',
            'options' => [
                'label' => 'Password',
            ],
            'attributes' => [
                'placeholder' => 'Password',
                'class' => 'form-control',
            ],
        ]);
    }
}