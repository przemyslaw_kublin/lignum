<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-21
 * Time: 09:13
 */

namespace Auth\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class LoginForm extends InputFilter {

    public function __construct()
    {
        $this->add([
            'name' => 'email',
            'required' => true,
        ]);

        $this->add([
            'name' => 'password',
            'required' => true,
        ]);
    }
}