<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;


use DoctrineModule\Authentication\Adapter\ObjectRepository;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Debug\Debug;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class IndexController extends MainController
{
    public function indexAction()
    {
        $this->redirect()->toRoute('auth/login');
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthService()
    {
        /** @var AuthenticationService $auth */
        $auth = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');

        return $auth;
    }

    public function loginAction()
    {
        $form = $this->getLoginForm();
        $message ='';
        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $auth = $this->getAuthService();

                // configure session storage
                /*
                if ($this->params()->fromPost('rememberMe', 0)) {
                    $ttl = 30 * 24 * 60 * 60;
                } else {
                    $ttl = 60 * 60;
                } */

                $ttl = 30 * 24 * 60 * 60;

                $sessionContainer = new Container();
                $sessionManager = $sessionContainer->getManager();
                $sessionManager->rememberMe($ttl);

                $authStorage = new SessionStorage(SessionStorage::NAMESPACE_DEFAULT, SessionStorage::MEMBER_DEFAULT, $sessionManager);

                $auth->setStorage($authStorage);

                /** @var ObjectRepository $adapter */
                $adapter = $auth->getAdapter();
                $adapter->setIdentity($this->params()->fromPost('email'))
                    ->setCredential($this->params()->fromPost('password'));

                //Authenticate
                $result = $auth->authenticate();

                if ($result->isValid()) {
                    return $this->redirect()->toRoute('cms');
                } else {
                    $message = 'Błąd logowania';
                }
            }
        }

        return [
            'form' => $form,
            'message' => $message
        ];
    }

    public function logoutAction()
    {
        $this->getAuthService()->clearIdentity();

        return $this->redirect()->toRoute('auth/message');
    }

    public function messageAction()
    {
        return ['message' => 'Wylogowano poprawnie'];
    }
}
