<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-11-20
 * Time: 21:21
 */

namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class MainController extends AbstractActionController {

    /**
     * @return array|object
     */
    protected function getEntityManager()
    {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }

    /**
     * @return LoginForm
     */
    protected function getLoginForm()
    {
        /** @var ArticleForm $form */
        $form = $this->getFormElementManager()->get('Auth\Form\LoginForm');

        return $form;
    }

    /**
     * @return FormElementManager
     */
    protected function getFormElementManager()
    {
        return $this->getServiceLocator()->get('FormElementManager');
    }
}