<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth;


use Zend\ModuleManager\ModuleManager;

class Module
{
    public function init(ModuleManager $mm)
    {
        $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__,
            'dispatch', function($e) {
                $e->getTarget()->layout('layout/auth');
            });
    }

    public function getConfig()
    {
        $config = [];
        $configFiles = scandir(__DIR__ . '/config');

        foreach ($configFiles as $file) {
            if (strpos($file, '.config.php')) {
                $config = array_merge_recursive($config, include __DIR__ . '/config/' . $file);
            }
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
