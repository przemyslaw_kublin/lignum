<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 15:34
 */

namespace Cms\Controller;


use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Products\Entity\Category;
use Products\Entity\Product;
use Products\Entity\ProductPhoto;
use Products\Form\ProductForm;
use Zend\Debug\Debug;

class ProductController extends MainController
{
    public function addAction()
    {
        /** @var ProductForm $form */
        $form = $this->getFormElementManager()->get('Products\Form\ProductForm');
        $categoryId = $this->params('id');
        /** @var Category $category */
        $category = $this->getEntityManager()->getRepository('Products\Entity\Category')->find($categoryId);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var Product $product */
                $product = new Product();
                $product->setName($form->get('name')->getValue())
                    ->setDescription($form->get('description')->getValue())
                    ->setCategory($category)
                    ->setCreated(new \DateTime());

                $this->getEntityManager()->persist($product);

                $files = $request->getFiles()->toArray();
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/';
                $allowedExt = [
                    'image/jpeg',
                    'image/png'
                ];
                foreach($files['filename'] as $file) {
                    if(!in_array($file['type'], $allowedExt)) {
                        continue;
                    }

                    if(!empty($file['tmp_name'])) {
                        $productPhoto = new ProductPhoto();

                        //save file
                        $filename = uniqid();
                        $newFilename = $path . $filename . '.jpg';
                        $newFilenameThumb = $path . $filename . '_thumb.jpg';
                        $image = new \Imagick($file['tmp_name']);

                        $image->thumbnailImage(1200, 1200, true);
                        $image->writeImage($newFilename);

                        $image->thumbnailImage(200, 200, true);
                        $image->writeImage($newFilenameThumb);

                        $productPhoto->setFilename($filename)
                            ->setProduct($product)
                            ->setCreated(new \DateTime());

                        $this->getEntityManager()->persist($productPhoto);
                    }
                }
                $this->getEntityManager()->flush();

                $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                $this->redirect()->toRoute('product/add', ['lang' => $this->params('lang'), 'id' => $categoryId]);
            }
        }

        return [
            'category' => $category,
            'form' => $form,
        ];
    }

    public function editAction()
    {
        $id = $this->params('id');

        /** @var Product $product */
        $product = $this->getEntityManager()->getRepository('Products\Entity\Product')->find($id);

        /** @var ProductForm $form */
        $form = $this->getFormElementManager()->get('Products\Form\ProductForm');
        $form->setHydrator(new DoctrineObject($this->getEntityManager()));
        $form->bind($product);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $product->setName($form->get('name')->getValue())
                    ->setDescription($form->get('description')->getValue())
                    ->setUpdated(new \DateTime());

                $this->getEntityManager()->persist($product);

                $files = $request->getFiles()->toArray();
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/';
                $allowedExt = [
                    'image/jpeg',
                    'image/png'
                ];
                foreach($files['filename'] as $file) {
                    if(!empty($file['tmp_name'])) {
                        if(!in_array($file['type'], $allowedExt)) {
                            continue;
                        }
                        $productPhoto = new ProductPhoto();

                        //save file
                        $filename = uniqid();
                        $newFilename = $path . $filename . '.jpg';
                        $newFilenameThumb = $path . $filename . '_thumb.jpg';
                        $image = new \Imagick($file['tmp_name']);

                        $image->thumbnailImage(1200, 1200, true);
                        $image->writeImage($newFilename);

                        $image->thumbnailImage(200, 200, true);
                        $image->writeImage($newFilenameThumb);

                        $productPhoto->setFilename($filename)
                            ->setProduct($product)
                            ->setCreated(new \DateTime());

                        $this->getEntityManager()->persist($productPhoto);
                    }
                }

                $this->getEntityManager()->flush();

                $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                $this->redirect()->toRoute('product/edit', ['lang' => $this->params('lang'), 'id' => $id]);
            }
        }

        return [
            'product' => $product,
            'form' => $form,
        ];
    }

    public function removeAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var Product $product */
            $product = $this->getEntityManager()->getRepository('Products\Entity\Product')->find($id);
            if(!empty($product->getPhoto())) {
                foreach($product->getPhoto() as $productPhoto) {
                    //remove image
                    $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/' . $productPhoto->getFilename() . '.jpg';
                    $thumb =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/' . $productPhoto->getFilename() . '_thumb.jpg';
                    unlink($path);
                    unlink($thumb);
                }
            }
            $this->getEntityManager()->remove($product);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));

            $this->redirect()->toRoute('product_category', ['lang' => $this->params('lang')]);
        }
    }

    public function removePhotoAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var ProductPhoto $productPhoto */
            $productPhoto = $this->getEntityManager()->getRepository('Products\Entity\ProductPhoto')->find($id);
            $productId = $productPhoto->getProduct()->getId();
            if(!empty($productPhoto->getFilename())) {
                //remove image
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/' . $productPhoto->getFilename() . '.jpg';
                $thumb =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/products/' . $productPhoto->getFilename() . '_thumb.jpg';
                unlink($path);
                unlink($thumb);
            }

            $this->getEntityManager()->remove($productPhoto);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));

            $this->redirect()->toRoute('product/edit', ['lang' => $this->params('lang'), 'id' => $productId]);
        } else {
            $this->flashMessenger()->addErrorMessage(_('Błąd'));
            $this->redirect()->toRoute('promotions', ['lang' => $this->params('lang')]);
        }
    }
}