<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-28
 * Time: 15:57
 */

namespace Cms\Controller;


use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Products\Entity\Category;
use Products\Form\CategoryForm;

class ProductCategoryController extends MainController
{
    public function indexAction()
    {
        /** @var \Products\Entity\Category $category */
        $category = $this->getEntityManager()->getRepository('Products\Entity\Category')->findBy(['lang' => $this->getCurrentLang()]);

        return [
            'category' => $category
        ];
    }

    public function addAction()
    {
        /** @var CategoryForm $form */
        $form = $this->getFormElementManager()->get('Products\Form\CategoryForm');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /**  @var \Products\Service\CategoryRepository $categoryRepo */
                $categoryRepo = $this->getServiceLocator()->get('Products\Service\CategoryRepository');
                /** @var \Products\Entity\Category $category */
                $category = $categoryRepo->getLastCategory($this->getCurrentLang());

                $position = 1;
                if($category) {
                    $position = ($category->getPosition() + 1);
                }

                /** @var Category $category */
                $category = new Category();
                $category->setName($form->get('name')->getValue())
                    ->setLang($this->getCurrentLang())
                    ->setPosition($position)
                    ->setCreated(new \DateTime());

                $this->getEntityManager()->persist($category);
                $this->getEntityManager()->flush($category);

                $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                $this->redirect()->toRoute('product_category/add', ['lang' => $this->params('lang')]);
            }
        }

        return [
            'form' => $form,
        ];
    }

    public function editAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var Category $category */
            $category = $this->getEntityManager()->getRepository('Products\Entity\Category')->find($id);

            /** @var CategoryForm $form */
            $form = $this->getFormElementManager()->get('Products\Form\CategoryForm');
            $form->setHydrator(new DoctrineObject($this->getEntityManager()));
            $form->bind($category);

            $request = $this->getRequest();

            if ($request->isPost()) {
                $form->setData($request->getPost());

                if ($form->isValid()) {

                    $category->setName($form->get('name')->getValue())
                        ->setUpdated(new \DateTime());

                    $this->getEntityManager()->persist($category);
                    $this->getEntityManager()->flush();

                    $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                    $this->redirect()->toRoute('product_category/edit', ['lang' => $this->params('lang'), 'id' => $id]);
                }
            }
        } else {
            $this->redirect()->toRoute('product_category', ['lang' => $this->params('lang')]);
        }
        return [
            'form' => $form
        ];
    }

    public function removeAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            //Remove item
            /** @var Category $category */
            $category = $this->getEntityManager()->getRepository('Products\Entity\Category')->find($id);

            $this->getEntityManager()->remove($category);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));
        } else {
            $this->flashMessenger()->addErrorMessage(_('Błąd'));
        }
        $this->redirect()->toRoute('product_category', ['lang' => $this->params('lang')]);
    }
}