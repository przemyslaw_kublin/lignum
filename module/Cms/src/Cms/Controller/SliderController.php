<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 16:26
 */

namespace Cms\Controller;


use Cms\Entity\Slider;
use Cms\Form\SliderForm;

class SliderController extends MainController
{
    public function indexAction()
    {
        /** @var SliderForm $form */
        $form = $this->getFormElementManager()->get('Cms\Form\SliderForm');

        /** @var \Cms\Entity\Slider $photos */
        $photos = $this->getEntityManager()->getRepository('Cms\Entity\Slider')->findAll();
        $request = $this->getRequest();

        if ($request->isPost()) {
            //$form->setData($request->getPost());
            $form->setData($request->getFiles());

            if ($form->isValid()) {
                $files = $request->getFiles()->toArray();
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/slider/';

                foreach($files['filename'] as $file) {
                    if(!empty($file['tmp_name'])) {
                        //Save file
                        $filename = uniqid();
                        $newFilename = $path . $filename . '.jpg';
                        $newFilenameThumb = $path . $filename . '_thumb.jpg';
                        $image = new \Imagick($file['tmp_name']);

                        $image->thumbnailImage(1900, 1900, true);
                        $image->writeImage($newFilename);

                        $image->thumbnailImage(200, 200, true);
                        $image->writeImage($newFilenameThumb);

                        /** @var Slider $slider */
                        $slider = new Slider();
                        $slider->setFilename($filename)
                            ->setCreated(new \DateTime());

                        $this->getEntityManager()->persist($slider);
                    }
                }
                $this->getEntityManager()->flush();

                $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                $this->redirect()->toRoute('slider', ['lang' => $this->params('lang')]);
            } else {
                var_dump($form->getMessages());
            }

        }

        return [
            'photos' => $photos,
            'form' => $form
        ];
    }

    public function removePhotoAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var Slider $slider */
            $slider = $this->getEntityManager()->getRepository('Cms\Entity\Slider')->find($id);

            if(!empty($slider->getFilename())) {
                //remove image
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/slider/' . $slider->getFilename() . '.jpg';
                $thumb =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/slider/' . $slider->getFilename() . '_thumb.jpg';
                unlink($path);
                unlink($thumb);
            }

            $this->getEntityManager()->remove($slider);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));
        } else {
            $this->flashMessenger()->addErrorMessage(_('Błąd'));
        }
        $this->redirect()->toRoute('slider', ['lang' => $this->params('lang')]);
    }
}