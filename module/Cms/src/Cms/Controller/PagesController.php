<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-26
 * Time: 11:17
 */

namespace Cms\Controller;

use \Pages\Form\StaticPageForm;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class PagesController extends MainController
{
    public function indexAction()
    {
        /** @var \Pages\Entity\Page $page */
        $pages = $this->getEntityManager()->getRepository('Pages\Entity\Page')->findBy(['lang' => $this->getCurrentLang()]);

        return [
            'pages' => $pages
        ];
    }

    public function addAction()
    {
        /** @var StaticPageForm $form */
        $form = $this->getFormElementManager()->get('Pages\Form\StaticPageForm');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));
                $this->redirect()->toRoute('static_pages/add', ['lang' => $this->params('lang')]);
            }
        }

        return [
            'form' => $form,
        ];
    }

    public function editAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var \Pages\Entity\Page $page */
            $page = $this->getEntityManager()->getRepository('\Pages\Entity\Page')->find($id);

            /** @var StaticPageForm $form */
            $form = $this->getFormElementManager()->get('Pages\Form\StaticPageForm');

            $form->setHydrator(new DoctrineObject($this->getEntityManager()));
            $form->bind($page);

            $request = $this->getRequest();

            if ($request->isPost()) {
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $page->setName($form->get('name')->getValue())
                        ->setContent($form->get('content')->getValue())
                        ->setUpdated(new \DateTime());

                    $this->getEntityManager()->persist($page);
                    $this->getEntityManager()->flush($page);

                    $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));

                    $this->redirect()->toRoute('static_pages/edit', ['lang' => $this->params('lang'), 'id' => $id]);
                }
            }
        } else {
            $this->redirect()->toRoute('static_pages', ['lang' => $this->params('lang')]);
        }

        return [
            'form' => $form,
        ];
    }

    public function removeAction()
    {

    }
}