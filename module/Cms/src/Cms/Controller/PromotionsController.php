<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-21
 * Time: 22:35
 */

namespace Cms\Controller;

use Cms\Form\PromotionForm;
use Cms\Entity\Promotion;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class PromotionsController extends MainController {

    public function indexAction()
    {
        /**  @var \Cms\Service\PromotionRepository $promotionRepo */
        $promotionRepo = $this->getServiceLocator()->get('Cms\Service\PromotionRepository');
        $promotions = $promotionRepo->getAllPromotions($this->getCurrentLang());

        $currentDate = new \DateTime();

        return [
            'currentDate' => $currentDate,
            'promotions' => $promotions
        ];
    }

    public function addAction()
    {
        /** @var PromotionForm $form */
        $form = $this->getFormElementManager()->get('Cms\Form\PromotionForm');

        /** @var Promotion $promotion */
        $promotion = new Promotion();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $files = $request->getFiles()->toArray();
            if(!empty($files['filename']['tmp_name'])) {
                $data = $data = array_merge($request->getPost()->toArray(), ['filename' => $this->params()->fromFiles('filename')]);
            } else {
                $data = $request->getPost();
            }
            $form->setData($data);

            $type = $form->get('type')->getValue();
            if($type == 's') {
                //Remove valid_to validator
                $form->getInputFilter()->remove('valid_to');
            }

            if ($form->isValid()) {
                $promotion->setTitle($form->get('title')->getValue())
                    ->setDescription($form->get('description')->getValue())
                    ->setLang($this->getCurrentLang())
                    ->setType($type)
                    ->setCreated(new \DateTime());

                if($type != 's') {
                    $validTo = new \DateTime($form->get('valid_to')->getValue());
                    $promotion->setValidTo($validTo);
                }

                if(!empty($files['filename']['tmp_name'])) {
                    $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/promotions/';
                    $filename = uniqid();

                    $newFilename = $path . $filename . '.jpg';
                    $image = new \Imagick($files['filename']['tmp_name']);

                    $image->thumbnailImage(400, 400, true);
                    $image->writeImage($newFilename);

                    $promotion->setFilename($filename);
                }

                $this->getEntityManager()->persist($promotion);
                $this->getEntityManager()->flush($promotion);

                $this->flashMessenger()->addSuccessMessage(_('Zapis poprawny'));
                $this->redirect()->toRoute('promotions/add', ['lang' => $this->params('lang')]);
            }
        }

        return [
            'form' => $form,
        ];
    }

    public function removeAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            //Remove item
            /** @var Promotion $promotion */
            $promotion = $this->getEntityManager()->getRepository('Cms\Entity\Promotion')->find($id);

            if(!empty($promotion->getFilename())) {
                //remove image
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/promotions/' . $promotion->getFilename() . '.jpg';
                unlink($path);
            }

            $this->getEntityManager()->remove($promotion);
            $this->getEntityManager()->flush();

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));
        } else {
            $this->flashMessenger()->addErrorMessage(_('Błąd'));
        }
        $this->redirect()->toRoute('promotions', ['lang' => $this->params('lang')]);
    }

    public function editAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            /** @var Promotion $promotion */
            $promotion = $this->getEntityManager()->getRepository('Cms\Entity\Promotion')->find($id);

            /** @var PromotionForm $form */
            $form = $this->getFormElementManager()->get('Cms\Form\PromotionForm');
            $form->setHydrator(new DoctrineObject($this->getEntityManager()));
            $form->bind($promotion);

            if(!empty($promotion->getValidTo())) {
                $form->setData(['valid_to' => $promotion->getValidTo()->format('Y-m-d')]);
            }

            $type = $promotion->getType();
            if($type == 's') {
                $form->get('valid_to')->setAttribute('disabled', 'disabled');
            }

            $request = $this->getRequest();
            if ($request->isPost()) {

                $files = $request->getFiles()->toArray();

                if(!empty($files['filename']['tmp_name'])) {
                    $data = $data = array_merge($request->getPost()->toArray(), ['filename' => $this->params()->fromFiles('filename')]);
                } else {
                    $data = $request->getPost();
                }

                $form->setData($data);

                $newType = $form->get('type')->getValue();
                if($newType == 's') {
                    //Remove valid_to validator
                    $form->getInputFilter()->remove('valid_to');
                } else {
                    $form->get('valid_to')->removeAttribute('disabled', 'disabled');
                }

                if ($form->isValid()) {
                    $promotion->setTitle($form->get('title')->getValue())
                        ->setDescription($form->get('description')->getValue())
                        ->setUpdated(new \DateTime());

                    if($newType == 's') {
                        $promotion->setValidTo(null);
                    }
                    else {
                        $validTo = new \DateTime($form->get('valid_to')->getValue());
                        $promotion->setValidTo($validTo);
                    }

                    if(!empty($files['filename']['tmp_name'])) {
                        $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/promotions/';
                        $filename = uniqid();

                        $newFilename = $path . $filename . '.jpg';
                        $image = new \Imagick($files['filename']['tmp_name']);

                        $image->thumbnailImage(400, 400, true);
                        $image->writeImage($newFilename);

                        $promotion->setFilename($filename);
                    }

                    $this->getEntityManager()->persist($promotion);
                    $this->getEntityManager()->flush();

                    $this->flashMessenger()->addSuccessMessage(_('Zapisano poprawnie'));

                    $this->redirect()->toRoute('promotions/edit', ['lang' => $this->params('lang'), 'id' => $id]);
                }
            }
        } else {
            $this->redirect()->toRoute('promotions', ['lang' => $this->params('lang')]);
        }

        return [
            'promotion' => $promotion,
            'form' => $form,
        ];
    }

    public function removeImageAction()
    {
        $id = $this->params('id');
        if (!empty($id)) {
            //Remove item
            /** @var Promotion $promotion */
            $promotion = $this->getEntityManager()->getRepository('Cms\Entity\Promotion')->find($id);

            if(!empty($promotion->getFilename())) {
                //remove image
                $path =  $_SERVER['DOCUMENT_ROOT'] . '/img/userfiles/promotions/' . $promotion->getFilename() . '.jpg';
                unlink($path);
            }

            $promotion->setFilename(null)
                ->setUpdated(new \DateTime());

            $this->getEntityManager()->persist($promotion);
            $this->getEntityManager()->flush($promotion);

            $this->flashMessenger()->addSuccessMessage(_('Usunięto poprawnie'));

            $this->redirect()->toRoute('promotions/edit', ['lang' => $this->params('lang'), 'id' => $id]);
        } else {
            $this->flashMessenger()->addErrorMessage(_('Błąd'));
            $this->redirect()->toRoute('promotions', ['lang' => $this->params('lang')]);
        }
    }
}