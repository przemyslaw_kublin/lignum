<?php
/**
 * Copyright: STORY DESIGN Sp. z o.o.
 * Author: Przemyslaw Kublin
 * Date: 2015-12-18
 * Time: 23:00
 */

namespace Cms\Controller;


use Zend\Mvc\Controller\AbstractActionController;

class MainController extends AbstractActionController {

    /**
     * @return array|object
     */
    protected function getEntityManager()
    {
        return $this->getServiceLocator()->get('doctrine');
    }

    /**
     * @return FormElementManager
     */
    protected function getFormElementManager()
    {
        return $this->getServiceLocator()->get('FormElementManager');
    }

    protected function getTranslatorHelper()
    {
        $translate = $this->getServiceLocator()->get('ViewHelperManager')->get('translate');

        return $translate;
    }

    /**
     * @return mixed|\Zend\Mvc\Controller\Plugin\Params
     */
    protected function getCurrentLang()
    {
        return $this->params('lang');
    }
}