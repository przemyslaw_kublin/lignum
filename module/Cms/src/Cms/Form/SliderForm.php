<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-02-01
 * Time: 21:43
 */

namespace Cms\Form;


use Cms\Form\InputFilter\SliderInputFilter;
use Zend\Form\Form;

class SliderForm extends Form
{
    public function __construct()
    {
        parent::__construct('promotion-form');

        $this->setInputFilter(new SliderInputFilter());

        $this->setAttributes([
            'method' => 'post',
            'enctype'=> 'multipart/form-data'
        ]);

        $this->add([
            'name' => 'filename',
            'type' => 'file',
            'options' => [
                'label' => _('Obrazek (.jpg, .png)'),
            ],
            'attributes' => [
                'type' => 'file',
                'class' => 'form-control',
                'multiple' => "multiple"
            ],
        ]);
    }
}