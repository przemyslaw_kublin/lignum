<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-17
 * Time: 21:21
 */

namespace Cms\Form;

use Cms\Form\InputFilter\PromotionInputFilter;
use Zend\Form\Form;

class PromotionForm extends Form
{
    public function __construct()
    {
        parent::__construct('promotion-form');

        $this->setInputFilter(new PromotionInputFilter());

        $this->setAttributes([
            'method' => 'post',
            'enctype'=> 'multipart/form-data'
        ]);

        $this->add([
            'name'  => 'type',
            'type'  => 'radio',
            'attributes' => [
                'class' => 'type-input',
                'value' => 'p'
            ],
            'options' => [
                'label' => _('Typ'),
                'value_options' => [
                    'promotion' => [
                        'value' => 'p',
                        'label' => _('Promocja'),
                    ],
                    'sale' => [
                        'value' => 's',
                        'label' => _('Wyprzedaż')
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'valid_to',
            'type' => 'text',
            'options' => [
                'label' => _('Ważna do'),
            ],
            'attributes' => [
                'class' => 'form-control valid-to',
            ],
        ]);

        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => _('Tytuł'),
            ],
            'attributes' => [
                'placeholder' => _('Tytuł'),
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => _('Treść'),
            ],
            'attributes' => [
                'placeholder' => _('Treść'),
                'class' => 'form-control',
                'id' => 'editor'
            ],
        ]);

        $this->add([
            'name' => 'filename',
            'type' => 'file',
            'attributes' => [
                'class' => 'form-control',
            ],
            'options' => [
                'label' => _('Obrazek (.jpg, .png)'),
            ],
        ]);
    }
}