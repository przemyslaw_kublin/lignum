<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-17
 * Time: 21:25
 */

namespace Cms\Form\InputFilter;


use Zend\InputFilter\InputFilter;

class SliderInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'filename',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
                [
                    'name' => 'Zend\Validator\File\Extension',
                    'options' => [
                        'extension' => 'jpg',
                        'messages' => [
                            'fileExtensionFalse' => _('Zły format pliku. Dopuszczalne: jpg, png')
                        ],
                    ],
                ]
            ],
        ]);
    }
}