<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-17
 * Time: 21:25
 */

namespace Cms\Form\InputFilter;


use Zend\InputFilter\InputFilter;

class PromotionInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'type',
            'required' => true,
        ]);

        $this->add([
            'name' => 'valid_to',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'title',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'description',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'filename',
            'required' => false,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
                [
                    'name' => 'Zend\Validator\File\Extension',
                    'options' => [
                        'extension' => 'jpg, png',
                        'messages' => [
                            'fileExtensionFalse' => _('Zły format pliku. Dopuszczalne: jpg, png')
                        ],
                    ],
                ]
            ],
        ]);
    }
}