<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-28
 * Time: 10:01
 */

namespace Cms\Service;

class PromotionRepository extends AbstractRepository
{
    /**
     * @param string $lang
     * @return array
     */
    public function getCurrentPromotions($lang)
    {
        $date = new \DateTime();

        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('p')
            ->from('Cms\Entity\Promotion', 'p')
            ->where('p.lang = :lang')
            ->andWhere('p.validTo >= :validTo')
            ->andWhere('p.type = :type')
            ->orderBy('p.validTo', 'asc')
            ->setParameters(['validTo'=> $date, 'lang' => $lang, 'type' => 'p'])
            ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getLastPromotions($lang)
    {
        $date = new \DateTime();

        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('p')
            ->from('Cms\Entity\Promotion', 'p')
            ->where('p.lang = :lang')
            ->andWhere('p.validTo >= :validTo')
            ->andWhere('p.type = :type')
            ->orderBy('p.validTo', 'asc')
            ->setMaxResults(4)
            ->setParameters(['validTo'=> $date, 'lang' => $lang, 'type' => 'p'])
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getAllPromotions($lang)
    {
        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('p')
            ->from('Cms\Entity\Promotion', 'p')
            ->where('p.lang = :lang')
            ->orderBy('p.validTo', 'asc')
            ->setParameters(['lang' => $lang])
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getCurrentSales($lang)
    {
        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('p')
            ->from('Cms\Entity\Promotion', 'p')
            ->where('p.lang = :lang')
            ->andWhere('p.type = :type')
            ->setParameters(['lang' => $lang, 'type' => 's'])
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $lang
     * @return array
     */
    public function getLastSales($lang)
    {
        $qb = $this->getDoctrine()->createQueryBuilder();
        $qb->select('p')
            ->from('Cms\Entity\Promotion', 'p')
            ->where('p.lang = :lang')
            ->andWhere('p.type = :type')
            ->setMaxResults(4)
            ->orderBy('p.created')
            ->setParameters(['lang' => $lang, 'type' => 's'])
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}