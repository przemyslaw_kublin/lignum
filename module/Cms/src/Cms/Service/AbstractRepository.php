<?php
/**
 * Copyright: STORY DESIGN Sp. z o.o.
 * Author: Przemyslaw Kublin
 * Date: 2016-01-28
 * Time: 10:04
 */

namespace Cms\Service;

use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as PaginationAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Paginator\Paginator;

abstract class AbstractRepository implements ServiceLocatorAwareInterface
{
    private $serviceLocator;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Get Doctrine EntityManager
     *
     * @return EntityManager
     */
    protected function getDoctrine()
    {
        return $this->getServiceLocator()->get('doctrine');
    }

    /**
     * @param $perPage
     * @param $page
     * @param $queryBuilder
     * @return Paginator
     */
    protected function createPaginator($perPage, $page, $queryBuilder)
    {
        $adapter = new PaginationAdapter(new ORMPaginator($queryBuilder->getQuery()));
        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage($perPage);
        return $paginator;
    }
}