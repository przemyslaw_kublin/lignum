<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-22
 * Time: 21:37
 */

namespace Cms\Entity;


use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="slider")
 */
class Slider extends AbstractEntity {

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $filename;

    /**
     * @param string $filename
     * @return Promotion
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }
}