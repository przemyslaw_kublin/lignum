<?php

return [
    'router' => [
        'routes' => [
            'cms' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/cms',
                    'defaults' => [
                        'controller' => 'Cms\Controller\Index',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
            ],
            'static_pages' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/cms/static-pages',
                    'defaults' => [
                        'controller' => 'Cms\Controller\Pages',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'add',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/edit[/:id]',
                            'defaults' => [
                                'action' => 'edit',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove[/:id]',
                            'defaults' => [
                                'action' => 'remove',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ],
            ],
            'promotions' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/cms/promotions',
                    'defaults' => [
                        'controller' => 'Cms\Controller\Promotions',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'add',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/edit[/:id]',
                            'defaults' => [
                                'action' => 'edit',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove[/:id]',
                            'defaults' => [
                                'action' => 'remove',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove_image' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove-image[/:id]',
                            'defaults' => [
                                'action' => 'remove-image',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ],
            ],
            'product_category' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/cms/product-category',
                    'defaults' => [
                        'controller' => 'Cms\Controller\ProductCategory',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'add',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/edit/:id',
                            'defaults' => [
                                'action' => 'edit',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove/:id',
                            'defaults' => [
                                'action' => 'remove',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ]
            ],
            'product' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/cms/product',
                    'defaults' => [
                        'controller' => 'Cms\Controller\Product',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/add/:id',
                            'defaults' => [
                                'action' => 'add',
                            ],
                        ],
                        'constraints' => [
                            'id' => '[0-9]*',
                        ],
                    ],
                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/edit[/:id]',
                            'defaults' => [
                                'action' => 'edit',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove/:id',
                            'defaults' => [
                                'action' => 'remove',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                    'remove_photo' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove-photo/:id',
                            'defaults' => [
                                'action' => 'remove-photo',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ]
            ],
            'slider' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/cms/slider',
                    'defaults' => [
                        'controller' => 'Cms\Controller\Slider',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'remove_photo' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/remove-photo/:id',
                            'defaults' => [
                                'action' => 'remove-photo',
                            ],
                            'constraints' => [
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ]
            ]
        ],
    ],
];