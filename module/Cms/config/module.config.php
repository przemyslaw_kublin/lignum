<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 22.10.2015
 * Time: 11:03
 */

namespace Cms;

return [
    'controllers' => [
        'invokables' => [
            'Cms\Controller\Index' => Controller\IndexController::class,
            'Cms\Controller\Promotions' => Controller\PromotionsController::class,
            'Cms\Controller\Pages' => Controller\PagesController::class,
            'Cms\Controller\ProductCategory' => Controller\ProductCategoryController::class,
            'Cms\Controller\Product' => Controller\ProductController::class,
            'Cms\Controller\Slider' => Controller\SliderController::class,
        ],
    ],

    'view_manager' => [
        'template_map' => [
            'layout/cms'           => __DIR__ . '/../view/layout/cms-layout.phtml',
            'cms/index/index' => __DIR__ . '/../view/cms/index/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'service_manager' => [
        'invokables' => [
            'Cms\Service\PromotionRepository' => 'Cms\Service\PromotionRepository',
        ],
        'aliases' => [
            'doctrine' => 'Doctrine\ORM\EntityManager',
        ],
    ],
];
