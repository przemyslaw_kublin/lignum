<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cms;


use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Zend\Http\Response;

class Module
{
    public $lang;

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        $response = $e->getResponse();
        if ($response instanceof Response) {
            $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH,
                function($e){
                    $params = $e->getRouteMatch()->getParams();
                    $this->lang = $params['lang'];
                }
            );

            $eventManager->attach('dispatch', [$this, 'setSlug']);
        }
    }

    public function setSlug(MvcEvent $e) {
        $e->getViewModel()->setVariables(
            [
                'lang' => $this->lang,
            ]
        );
    }

    public function init(ModuleManager $mm)
    {
        $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__,
            'dispatch', function($e) {
                $e->getTarget()->layout('layout/cms');
            });
    }

    public function getConfig()
    {
        $config = [];
        $configFiles = scandir(__DIR__ . '/config');

        foreach ($configFiles as $file) {
            if (strpos($file, '.config.php')) {
                $config = array_merge_recursive($config, include __DIR__ . '/config/' . $file);
            }
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
