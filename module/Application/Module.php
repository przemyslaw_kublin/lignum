<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Listener\ProductsListener;
use Application\Listener\PromotionsListener;
use Application\Listener\SliderListener;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Listener\RouterListener;
use Zend\Http\Response;

class Module
{
    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $eventManager->attach('route', [$this, 'loadConfiguration'], 2);

        /** @var ServiceManager $serviceManager */
        $serviceManager = $e->getApplication()->getServiceManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(new RouterListener($serviceManager));
        $eventManager->attach(new ProductsListener($serviceManager));
        $eventManager->attach(new SliderListener($serviceManager));
        $eventManager->attach(new PromotionsListener($serviceManager));

        $eventManager->attach('route', [$this, 'setLocale'], 20);
    }

    public function loadConfiguration(MvcEvent $e)
    {
        $application   = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $router = $sm->get('router');
        $request = $sm->get('request');

        $response = $e->getResponse();
        if ($response instanceof Response) {
            $matchedRoute = $router->match($request);
            if (null !== $matchedRoute) {
                $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController','dispatch',
                    function($e) use ($sm) {
                        $sm->get('ControllerPluginManager')->get('AclPlugin')
                            ->doAuthorization($e);
                    },2
                );
            }
        }
    }

    public function getConfig()
    {
        $config = [];
        $configFiles = scandir(__DIR__ . '/config');

        foreach ($configFiles as $file) {
            if (strpos($file, '.config.php')) {
                $config = array_merge_recursive($config, include __DIR__ . '/config/' . $file);
            }
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        );
    }

    //Set language
    public function setLocale(MvcEvent $e) {
        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH,
            function($e) {
                if($params = $e->getRouteMatch()->getParams()) {
                    if(isset($params['lang'])) {
                        $slug = $params['lang'];
                    }
                    else {
                        $slug = 'pl';
                    }
                    switch ($slug) {
                        case 'pl':
                            $e->getApplication()->getServiceManager()->get('translator')->setLocale('pl_PL');
                            break;
                        case 'en':
                            $e->getApplication()->getServiceManager()->get('translator')->setLocale('en_GB');
                            break;
                        case 'de':
                            $e->getApplication()->getServiceManager()->get('translator')->setLocale('de_DE');
                            break;
                    }
                }
            }
        );
    }
}
