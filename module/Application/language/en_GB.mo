��    C      4  Y   L      �     �     �     �     �     �     �     	     '     6     C  G   X     �     �     �     �     �     �     �  
   �                1     D     R     X     e     k     �     �  	   �     �     �     �  >   �          $     -     6     L  G   `     �     �     �     �  1   �     	     	     	     	     -	     1	     8	  	   L	     V	     r	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	  )   
  �  B
     !     (     4     :     U     b     n     �     �     �  A   �     �     �       	                  1  
   9     D     K     Y     f     v  	   {     �     �     �     �  	   �     �     �     �  1   �     -  	   6     @     N     a  :   r     �     �  	   �     �     �     �     �     �                          .     7     K     X     i     p     u     z     �     �     �     �     �  -   �     '          (       8             
   1   #         B   &   "   ,   A   *                 !                3   .   :             6   =   9       )         5         @   	   <               2   C              %   ?               +          /      4   -      0           7   >                             ;                 $    Akcja Brak produktów Błąd Błędny format adresu Email Dodaj kategorie Dodaj produkt Dodaj produkt do kategori: %s Dodaj promocje Dodaj strone Dowiedz się więcej Dziękujęmy za wysłanie maila. Odpowiemy najszybciej jak to możliwe. Edytuj Edytuj kategorie Edytuj produkt Edytuj strone Język Kategorie produktów Kontakt Lignum CMS Lignum wyposażenie wnętrz Lista jest pusta Logowanie - Lignum Napisz do nas Nazwa Nie aktualne O nas Obrazek (.jpg, .png) Obrazki (.jpg, .png) Opis Panel CMS Pole nie może być puste Polityka prywatności Powrót Pozostając na niej, wyrażasz zgodę na korzystanie z cookies Produkty Promocja Promocje Promocje / Wyprzedaż Promocje/Wyprzedaż Przepraszamy. Wystąpił błąd z wysłaniem maila. Spróbuj później. Slider Slug Strona główna Strony statyczne Ta strona wykorzystuje pliki cookie (ciasteczka)  Temat Treść Tutaj Twój adres email Typ Tytuł Usunięto poprawnie Ważna do Wszelkie prawa zastrzeżone Wszystkie produkty Wylogowano poprawnie Wyloguj Wyprzedaż Wyślij Zaloguj Zaloguje ponownie Zapis poprawny Zapisano poprawnie Zapisz Zostałeś wylogowany poprawnie Zły format pliku. Dopuszczalne: jpg, png Project-Id-Version: 
POT-Creation-Date: 2016-02-25 13:56+0100
PO-Revision-Date: 2016-02-25 13:56+0100
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: translate;_
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: Application/language
 Action No products Error Wrong email address format Add category Add product Add product to category: %s Add promotion Add page See more at Thank you for send message to us. We answer  as soon as possible. Edit Edit category Edit product Edit site Lang Product's category Contact Lignum CMS Lignum List is empty Lignum login Send mail to us Name Not valid About us Image (.jpg, .png) Images (.jpg, .png) Description Panel CMS This field can not be empty Privacy policy Back By staying on it, you agree to the use of cookies Products Promotion Special offer Promotions / Sales Promotions/Sales Sorry. Error occured during mail sending. Try again later. Slider Slug Home page Static pages This site use cookie fiels Subject Content Here Your email address Type Title Removed correct Valid to All rights reserved All products Logout correctly Logout Sale Send Log in Login again Save correct Save correct Save Logout correctly Invalid file extension. You can add: jpg, png 