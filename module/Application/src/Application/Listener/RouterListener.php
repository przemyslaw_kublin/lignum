<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-11-20
 * Time: 22:28
 */

namespace Application\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;

class RouterListener implements ListenerAggregateInterface {

    /**
     * @var array
     */
    protected $listeners = [];

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceManager;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('*', array($this, 'getRouterName'));
    }

    /**
     * @param MvcEvent $event
     */
    public function getRouterName(MvcEvent $event) {

        $router = $this->serviceManager->get('router');
        $request = $this->serviceManager->get('request');

        $matchedRoute = $router->match($request);

        if($matchedRoute) {
            $route = $matchedRoute->getMatchedRouteName();
            $event->getViewModel()->setVariable('route', $route);
        } else {
            $event->getViewModel()->setVariable('route', 'home');
        }
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
}