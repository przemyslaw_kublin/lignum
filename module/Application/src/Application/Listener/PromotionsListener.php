<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-02-02
 * Time: 12:12
 */

namespace Application\Listener;


use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;

class PromotionsListener implements ListenerAggregateInterface
{
    /**
     * @var array
     */
    protected $listeners = [];

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceManager;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('*', array($this, 'getPromotions'));
    }

    /**
     * @param MvcEvent $event
     */
    public function getPromotions(MvcEvent $event) {
        $router = $this->serviceManager->get('router');
        $request = $this->serviceManager->get('request');

        /** @var \Zend\Mvc\Router\Http\RouteMatch $matchedRoute */
        $matchedRoute = $router->match($request);
        $lang = $matchedRoute->getParam('lang');
        $controller = $matchedRoute->getParam('controller');

        $moduleArray = explode('\\', $controller);
        $module = $moduleArray[0];

        if($module !== 'Cms') {
            /** @var \Cms\Service\PromotionRepository $promotionRepo */
            $promotionRepo = $this->serviceManager->get('Cms\Service\PromotionRepository');
            $promotions = $promotionRepo->getLastPromotions($lang);

            $sales = $promotionRepo->getLastSales($lang);

            $event->getViewModel()->setVariables(['smallPromotionList' => $promotions, 'smallSaleList' => $sales]);
        }
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
}