<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-27
 * Time: 15:08
 */

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class StrCut extends AbstractHelper
{
    public function __invoke($string, $width = 100)
    {
        $end = '';
        if(strlen($string) > $width) {
            $string = wordwrap($string, $width);
            $string = substr($string, 0, strpos($string, "\n"));

            $end = '...';
        }

        return $string . $end;
    }
}