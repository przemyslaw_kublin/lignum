<?php 
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
 
class Charchange extends AbstractHelper
{
    public function __invoke($str)
    {
        $aTable = [
	        'ą'=>'a', 'ę'=>'e', 'ś'=>'s', 'ó'=>'o', 'ł'=>'l', 'ż'=>'z', 'ź'=>'z', 'ć'=>'c', 'ń'=>'n', 
			'Ą'=>'a', 'Ę'=>'e', 'Ś'=>'s', 'Ó'=>'o', 'Ł'=>'l', 'Ż'=>'z', 'Ź'=>'z', 'Ć'=>'c', 'Ń'=>'n',
	        ' '=>'-', '/'=>'-', ','=>'-', '('=>'-', ')'=>'-', '.'=>'-'
		];
    	return strtolower(strtr($str, $aTable));
    }
}