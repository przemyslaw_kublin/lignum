<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-21
 * Time: 08:58
 */

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Messenger extends AbstractHelper
{
    public function __invoke()
    {
        $flash = $this->view->flashMessenger();
        $flash->setMessageOpenFormat('<div%s><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>')
            ->setMessageSeparatorString('</li><li>')
            ->setMessageCloseString('</li></ul></div>');

        $message = '<div class="messenger">';
        $message .= $flash->render('error',   ['alert', 'alert-dismissible', 'alert-danger']);
        $message .= $flash->render('info',    ['alert', 'alert-dismissible', 'alert-info']);
        $message .= $flash->render('default', ['alert', 'alert-dismissible', 'alert-warning']);
        $message .= $flash->render('success', ['alert', 'alert-dismissible', 'alert-success']);
        $message .='</div>';
        return $message;
    }
}