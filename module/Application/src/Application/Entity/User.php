<?php
/**
 * Copyright: Web onward
 * Author: Przemyslaw Kublin
 * Date: 2015-10-22
 * Time: 11:10
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Crypt\Password\Bcrypt;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user")
 */
class User extends AbstractEntity {

    /**
     * @ORM\Column(type="string")
    */
    protected $firstname;

    /**
     * @ORM\Column(type="string")
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @param string $firstname
     * @return Users
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $lastname
     * @return Users
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email.
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email.
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password.
     * @param string $password
     * @param bool $encrypt - crypt password with Bcrypt
     * @return User
     */
    public function setPassword($password, $encrypt = false)
    {
        if ($encrypt) {
            $password = (new Bcrypt())->create($password);
        }

        $this->password = $password;
        return $this;
    }

    /**
     * Get password.
     * @return string password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
} 