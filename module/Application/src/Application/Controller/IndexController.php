<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;


class IndexController extends MainController
{
    public function indexAction()
    {
        /** @var \Pages\Entity\Page $page */
        $page = $this->getEntityManager()->getRepository('Pages\Entity\Page')->findOneBy(['slug' => 'o-nas', 'lang' => $this->getCurrentLang()]);

        return [
            'page' => $page
        ];
    }
}
