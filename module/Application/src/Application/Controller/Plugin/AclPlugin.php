<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-21
 * Time: 11:31
 */

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin,
    Zend\Permissions\Acl\Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;

use Zend\Authentication\AuthenticationService;

class AclPlugin extends AbstractPlugin {
    
    protected $role;

    private function setRoles()
    {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity()) {
            $this->role = $auth->getIdentity()->getRole();
        } else {
            $this->role = 'guest';
        }
        return $this->role;
    }

    public function doAuthorization($e)
    {
        $acl = new Acl();

        $acl->addRole(new Role('guest'));
        $acl->addRole(new Role('admin'),  'guest');

        $acl->addResource(new Resource('Application'));
        $acl->addResource(new Resource('Auth'));
        $acl->addResource(new Resource('Cms'));

        $acl->allow('guest', ['Application', 'Auth']);
        $acl->allow('admin', ['Cms']);

        $controller = $e->getTarget();
        $controllerClass = get_class($controller);
        $namespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));

        $sRole = $this->setRoles();

        $router = $e->getRouter();
        if (!$acl->isAllowed($sRole, $namespace)) {
            $url = $router->assemble([], ['name' => 'auth']);

            $response = $e->getResponse();
            $response->setStatusCode(303);

            //redirect to login route...
            $response->getHeaders()->addHeaderLine('Location', $url);
            $e->stopPropagation();
        }
    }
}