<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-11-20
 * Time: 21:21
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class MainController extends AbstractActionController {

    /**
     * @return array|object
     */
    protected function getEntityManager()
    {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }

    /**
     * @return FormElementManager
     */
    protected function getFormElementManager()
    {
        return $this->getServiceLocator()->get('FormElementManager');
    }

    protected function getTranslatorHelper()
    {
        $translate = $this->getServiceLocator()->get('ViewHelperManager')->get('translate');

        return $translate;
    }

    /**
     * @return mixed|\Zend\Mvc\Controller\Plugin\Params
     */
    protected function getCurrentLang()
    {
        return $this->params('lang');
    }
}