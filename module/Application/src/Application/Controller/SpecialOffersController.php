<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-11-20
 * Time: 21:24
 */

namespace Application\Controller;

use Cms\Entity\Promotion;

class SpecialOffersController extends MainController {

    public function indexAction()
    {
        /**  @var \Cms\Service\PromotionRepository $promotionRepo */
        $promotionRepo = $this->getServiceLocator()->get('Cms\Service\PromotionRepository');
        $promotions = $promotionRepo->getCurrentPromotions($this->getCurrentLang());

        return [
            'promotions' => $promotions
        ];
    }

    public function detailsAction()
    {
        $id = $this->params('id');

        /** @var Promotion $promotion */
        $promotion = $this->getEntityManager()->getRepository('Cms\Entity\Promotion')->find($id);

        if(!is_object($promotion)) {
            $this->flashMessenger()->addErrorMessage('Błędne dane');

            $this->redirect()->toRoute('home');
        }

        return [
            'promotion' => $promotion
        ];
    }
}