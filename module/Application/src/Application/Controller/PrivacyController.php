<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-02-02
 * Time: 09:39
 */

namespace Application\Controller;


class PrivacyController extends MainController
{
    public function indexAction()
    {
        /** @var \Pages\Entity\Page $page */
        $page = $this->getEntityManager()->getRepository('Pages\Entity\Page')->findOneBy(['slug' => 'polityka-prywatnosci', 'lang' => $this->getCurrentLang()]);

        return [
            'page' => $page
        ];
    }

}