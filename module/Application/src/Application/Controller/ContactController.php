<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-29
 * Time: 14:24
 */

namespace Application\Controller;

class ContactController extends MainController
{
    public function indexAction()
    {
        //Get content
        /** @var \Pages\Entity\Page $page */
        $page = $this->getEntityManager()->getRepository('Pages\Entity\Page')->findOneBy(['slug' => 'kontakt', 'lang' => $this->getCurrentLang()]);

        /** @var ArticleForm $form */
        $form = $this->getFormElementManager()->get('Application\Form\ContactForm');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $addTo = "lignum@lignum.com.pl";
                $title = "Wysłanie formularza ze strony www.lignum.com.pl";
                $content = "Adres: ".$form->get('email')->getValue()."<br />";
                $content .="Temat: ".$form->get('title')->getValue()."<br />";
                $content .="Treść: ".$form->get('content')->getValue()."<br />";

                $headers = "From: www.lignum.com.pl <www.lignum.com.pl>\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n";

                if(mail($addTo, $title, $content, $headers)) {
                    $this->flashMessenger()->addSuccessMessage(_('Dziękujęmy za wysłanie maila. Odpowiemy najszybciej jak to możliwe.'));
                } else {
                    $this->flashMessenger()->addErrorMessage(_('Przepraszamy. Wystąpił błąd z wysłaniem maila. Spróbuj później.'));
                }

                $this->redirect()->toRoute('contact', ['lang' => $this->params('lang')]);
            }
        }

        return [
            'page' => $page,
            'form' => $form,
        ];
    }
}