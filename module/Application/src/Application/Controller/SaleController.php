<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-31
 * Time: 16:30
 */

namespace Application\Controller;


class SaleController extends MainController
{
    public function indexAction()
    {
        /**  @var \Cms\Service\PromotionRepository $promotionRepo */
        $promotionRepo = $this->getServiceLocator()->get('Cms\Service\PromotionRepository');
        $sales = $promotionRepo->getCurrentSales($this->getCurrentLang());

        return [
            'sales' => $sales
        ];
    }

    public function detailsAction()
    {
        $id = $this->params('id');

        /** @var Promotion $sale */
        $sale = $this->getEntityManager()->getRepository('Cms\Entity\Promotion')->find($id);

        if(!is_object($sale)) {
            $this->flashMessenger()->addErrorMessage('Błędne dane');

            $this->redirect()->toRoute('home');
        }

        return [
            'sale' => $sale
        ];
    }
}