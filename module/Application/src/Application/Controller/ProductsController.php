<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-02-01
 * Time: 10:35
 */

namespace Application\Controller;


class ProductsController extends MainController
{
    public function allAction()
    {

    }
    public function detailsAction()
    {
        $id = $this->params('id');
        //Get content
        /** @var \Products\Entity\Product $product */
        $product = $this->getEntityManager()->getRepository('Products\Entity\Product')->find($id);

        return [
            'product' => $product
        ];
    }
}