<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-29
 * Time: 14:30
 */

namespace Application\Form\InputFilter;


use Zend\InputFilter\InputFilter;

class ContactForm extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'title',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'email',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'messages' => [
                            'emailAddressInvalidFormat' => _("Błędny format adresu Email")
                        ],
                    ]
                ]
            ],
        ]);

        $this->add([
            'name' => 'content',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);
    }
}