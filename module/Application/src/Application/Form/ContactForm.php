<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2015-12-29
 * Time: 14:28
 */

namespace Application\Form;


use Zend\Form\Form;
use \Application\Form\InputFilter\ContactForm as ContactInputFilter;

class ContactForm extends Form
{
    public function __construct()
    {
        parent::__construct('contactForm');

        $this->setInputFilter(new ContactInputFilter());

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => _('Temat'),
            ],
            'attributes' => [
                'placeholder' => _('Temat'),
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'email',
            'type' => 'text',
            'options' => [
                'label' => _('Twój adres email'),
            ],
            'attributes' => [
                'placeholder' => _('Twój adres email'),
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'content',
            'type' => 'textarea',
            'options' => [
                'label' => _('Treść'),
            ],
            'attributes' => [
                'placeholder' => _('Treść'),
                'class' => 'form-control',
            ],
        ]);
    }
}