<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 22.10.2015
 * Time: 11:03
 */

namespace Application;

return [
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'factories' => [
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
            'Zend\Authentication\AuthenticationService' => function (\Zend\ServiceManager\ServiceManager $serviceLocatorInterface) {
                /** @var \Zend\Authentication\AuthenticationService $service */
                return $serviceLocatorInterface->get('doctrine.authenticationservice.orm_default');
            },
        ],
    ],
    'translator' => [
        'locale' => 'pl_PL',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Application\Controller\Index' => Controller\IndexController::class,
            'Application\Controller\SpecialOffers' => Controller\SpecialOffersController::class,
            'Application\Controller\Sale' => Controller\SaleController::class,
            'Application\Controller\Contact' => Controller\ContactController::class,
            'Application\Controller\Products' => Controller\ProductsController::class,
            'Application\Controller\Privacy' => Controller\PrivacyController::class
        ],
    ],
    'controller_plugins' => [
        'invokables' => [
            'AclPlugin' => 'Application\Controller\Plugin\AclPlugin',
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => false,
        'display_exceptions'       => false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'view_helpers' => [
        'invokables'=> [
            'messenger' => 'Application\View\Helper\Messenger',
            'charchange' => 'Application\View\Helper\Charchange',
            'strCut' => 'Application\View\Helper\StrCut',
        ]
    ],

    // Placeholder for console routes
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
];
