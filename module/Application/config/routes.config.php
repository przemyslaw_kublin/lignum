<?php

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '/[:lang]',
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
            ],
            'special_offer' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/promocje',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'SpecialOffers',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'details' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '[/:title][/:id]',
                            'constraints' => [
                                'title' => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                                'id' => '[0-9]*',
                            ],
                            'defaults' => [
                                'action' => 'details',
                            ],
                        ],
                    ],
                ],
            ],
            'sale' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/wyprzedaz',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Sale',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'details' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/:title/:id',
                            'constraints' => [
                                'title' => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                                'id' => '[0-9]*',
                            ],
                            'defaults' => [
                                'action' => 'details',
                            ],
                        ],
                    ],
                ],
            ],
            'contact' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/kontakt',
                    'defaults' => [
                        'controller' => 'Application\Controller\Contact',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
            ],
            'privacy_policy' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '[/:lang]/polityka-prywatnosci',
                    'defaults' => [
                        'controller' => 'Application\Controller\Privacy',
                        'action' => 'index',
                        'lang' => 'pl'
                    ],
                    'constraints' => [
                        'lang' => '(pl|en|de)?',
                    ],
                ],
            ],
            'products' => [
                'type'    => 'Segment',
                'options' => [
                    'route' => '[/:lang]/produkty',
                    'defaults' => [
                        'controller' => 'Application\Controller\Products',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'details' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/:title/:id',
                            'defaults' => [
                                'action' => 'details',
                                'lang' => 'pl',
                                'title' => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                                'id' => '[0-9]*',
                            ],
                            'constraints' => [
                                'lang' => '(pl|en|de)?',
                                'id' => '[0-9]*',
                            ],
                        ],
                    ],
                ],

            ],
        ],
    ],
];