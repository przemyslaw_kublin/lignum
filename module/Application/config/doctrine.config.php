<?php
/**
 * Copyright: STORY DESIGN Sp. z o.o.
 * Author: Yaroslav Shatkevich
 * Date: 28.10.2015
 * Time: 09:30
 */

return [
    'doctrine' => [
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Application\Entity\User',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => function (\Application\Entity\User $user, $providedPassword) {
                    return (new \Zend\Crypt\Password\Bcrypt())->verify($providedPassword, $user->getPassword());
                }
            ],
        ],
        'driver' => [
            'application_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Application/Entity'
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'application_driver',
                ],
            ],
        ],
        'fixture' => [
            'application_fixture' => __DIR__ . '/../src/Application/Entity/Fixture'
        ],
    ],
];