<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Pages;


class Module
{
    public function getConfig()
    {
        $config = [];
        $configFiles = scandir(__DIR__ . '/config');

        foreach ($configFiles as $file) {
            if (strpos($file, '.config.php')) {
                $config = array_merge_recursive($config, include __DIR__ . '/config/' . $file);
            }
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
