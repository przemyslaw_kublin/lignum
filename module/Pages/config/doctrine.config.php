<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 28.10.2015
 * Time: 09:30
 */

return [
    'doctrine' => [
        'driver' => [
            'pages_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Pages/Entity'
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Pages\Entity' => 'pages_driver',
                ],
            ],
        ],
    ],
];