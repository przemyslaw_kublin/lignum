<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-26
 * Time: 11:38
 */

namespace Pages\Form\InputFilter;

use Zend\InputFilter\InputFilter;

class StaticPageInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'content',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            'isEmpty' => _('Pole nie może być puste')
                        ],
                    ]
                ],
            ],
        ]);
    }
}