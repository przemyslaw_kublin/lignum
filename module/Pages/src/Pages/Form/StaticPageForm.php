<?php
/**
 * Copyright: Web Onward
 * Author: Przemyslaw Kublin
 * Date: 2016-01-26
 * Time: 11:36
 */

namespace Pages\Form;

use Pages\Form\InputFilter\StaticPageInputFilter;
use Zend\Form\Form;

class StaticPageForm extends Form
{
    public function __construct()
    {
        parent::__construct('static-page-form');

        $this->setInputFilter(new StaticPageInputFilter());

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => _('Nazwa'),
            ],
            'attributes' => [
                'class' => 'form-control name',
            ],
        ]);

        $this->add([
            'name' => 'slug',
            'type' => 'text',
            'options' => [
                'label' => _('Slug'),
            ],
            'attributes' => [
                'placeholder' => _('Slug'),
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'content',
            'type' => 'textarea',
            'options' => [
                'label' => _('Treść'),
            ],
            'attributes' => [
                'placeholder' => _('Treść'),
                'class' => 'form-control',
                'id' => 'editor'
            ],
        ]);
    }
}