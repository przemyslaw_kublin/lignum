function initMap() {
    var myLatLng = {lat: 50.0872563, lng: 19.9353027};
    var mapDiv = document.getElementById('map');

    var map = new google.maps.Map(mapDiv, {
        center: myLatLng,
        zoom: 17,
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Lignum'
    });
}