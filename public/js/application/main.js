$('img').addClass('img-responsive');
$('img').removeAttr('height');
$('img').removeAttr('width');

$( document ).ready(function() {
    setMargin();
    $(".fancybox").fancybox();

    //Close cookie info box
    var $cookieMsg = $('.cookie-info-box');
    if (document.cookie.replace(/(?:(?:^|.*;\s*)acceptedCookies\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
        $cookieMsg.removeClass('closed');
    }
    $cookieMsg.on('click', '.close-cookie-trigger', function() {
        $cookieMsg.addClass('closed');
        document.cookie = "acceptedCookies=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
    });
})

$( window ).resize(function() {
    setMargin();
});

/**
 * Set main content margin
 */
function setMargin() {
    var windowWidth = $(window).width();
    var sliderHeight = $('.carousel').height();
    var menuHeight = $('nav.navbar').height();

    if(windowWidth >= 768) {
        $('#content').css('margin-top', sliderHeight+menuHeight);
    } else {
        $('#content').css('margin-top', 60);
    }

    $('#content').show();
    $('.promotions-box').show();
    $('footer').show();
}

$('.products-trigger').click(function(e) {
    e.preventDefault();

    $('nav.navbar ul.nav li').removeClass('active');
    $(this).parent().addClass('active');

    $('.products-menu').addClass('open');
});

//click anywhere
$(window).click(function(e) {
    var targetId = e.target.id;
    if( (targetId !== 'products-trigger') && (targetId !== 'products-menu'))
    {
        $('.products-menu').removeClass('open');
    }
});
