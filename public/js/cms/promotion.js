$('.valid-to').datepicker({
    startDate: '0',
    format: 'yyyy-mm-dd',
    clearBtn: true,
    todayBtn: "linked",
    todayHighlight: true
});

$('.type-input').click(function() {
    var value = $(this).val();

    if(value == 's') {
        $('.valid-to').attr('disabled', 'disabled');
    } else {
        $('.valid-to').removeAttr('disabled');
    }
});